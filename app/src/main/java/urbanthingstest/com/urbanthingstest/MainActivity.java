package urbanthingstest.com.urbanthingstest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final TextView tickCounter = findViewById(R.id.tickCounter);

        final EditText weights = findViewById(R.id.weightInput);
        final EditText floors = findViewById(R.id.floorInput);
        final EditText maxPeople = findViewById(R.id.maxPeopleInput);
        final EditText maxWeight  = findViewById(R.id.maxWeightInput);
        Button calculate = findViewById(R.id.calculate);

        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String weightsList = weights.getText().toString();
                String floorList = floors.getText().toString();
                String mWeight = maxWeight.getText().toString();
                String mPeople = maxPeople.getText().toString();

                try {
                    List<String> wList = Arrays.asList(weightsList.split(","));
                    List<String> fList = Arrays.asList(floorList.split(","));
                    int maxWeight = Integer.valueOf(mWeight);
                    int maxPeople = Integer.valueOf(mPeople);
                    int maxFloors = 5;
                    ArrayList<Integer> peopleWeights = new ArrayList<>();
                    ArrayList<Integer> peopleFloors = new ArrayList<>();

                    convertToIntList(peopleWeights, wList);
                    convertToIntList(peopleFloors, fList);

                    int totalTicks = calculateLiftTicks(peopleWeights, peopleFloors, maxFloors, maxPeople, maxWeight);
                    if(totalTicks != -1) {
                        tickCounter.setText("Result: " + totalTicks);
                    }
                    else {
                        tickCounter.setText("Invalid inputs");
                    }

                } catch (Exception e){
                    e.printStackTrace();
                    tickCounter.setText("Invalid list inputs");
                }

            }
        });
    }

    public void convertToIntList(List<Integer> intList, List<String> sList) {
        for(String s : sList) {
            intList.add(Integer.valueOf(s));
        }
    }
    public int calculateLiftTicks(List<Integer> peopleWeights, List<Integer> peopleFloors, int maxFloors, int maxPeople, int maxWeight) {
        if (peopleWeights == null || peopleFloors == null) {
            return -1;
        }

        if (peopleFloors.size() != peopleWeights.size()) {
            return -1;
        }

        List<Person> people = new ArrayList<>();
        for (int i = 0; i < peopleWeights.size(); i++) {
            people.add(new Person(peopleWeights.get(i), peopleFloors.get(i)));
        }

        int remainingPassengers = people.size();
        Log.e("total count", "" + remainingPassengers);
        int ticks = 0;
        List<Person> currentLoad = new ArrayList<>();

        while (remainingPassengers > 0) {
            int loadingIndex = 0;
            int liftLoad = 0;
            currentLoad.clear();
           Log.e("left in queue", "" + people.size());

            while (liftLoad <= maxWeight) {
                if (currentLoad.size() != maxPeople && loadingIndex <= people.size()) {
                    Person person;
                    try {
                        person = people.get(loadingIndex);
                    } catch(Exception e) {
                        break;
                    }
                    liftLoad += person.getWeight();
                    currentLoad.add(person);
                    loadingIndex++;

                } else {
                    break;
                }
            }

            for (int i = loadingIndex - 1; i >= 0; i--) {
              //  Log.e("removing", i + " " + people.get(i).getWeight());
                people.remove(i);
            }

            //Log.e("lift loaded", "" + loadingIndex);
            ticks = incrementTicks(ticks, 1); // loaded lift
            ticks = moveLift(ticks, currentLoad);
            remainingPassengers -= currentLoad.size();
          //  Log.e("count", "" + remainingPassengers);
        }
      //  Log.e("ticks", "" + ticks);
        return ticks;
    }

    private int moveLift(int ticks, List<Person> currentLoad) {
        int liftCurrentFloor = 0;
        for (int x = 0; x < currentLoad.size(); x++) {
            int requestedFloor = currentLoad.get(x).getFloor();
            int floorDiff = requestedFloor - liftCurrentFloor;
            liftCurrentFloor += floorDiff;

            ticks = incrementTicks(ticks, Math.abs(floorDiff));  //moved X floors
            ticks = incrementTicks(ticks, 1); //offloading passenger

            //   Log.e("current passengerweight", " " + currentLoad.get(x).getWeight());
            //  Log.e("moved to floor", "" + liftCurrentFloor);
            // Log.e("offloading", "passenger");
        }
        //     Log.e("moving", "ground floor");
        ticks = incrementTicks(ticks, liftCurrentFloor); //moving to ground floor
        return ticks;
    }


    public int incrementTicks(int ticks, int increment) {
        ticks += increment;
        Log.e("current ticks", "" + ticks);
        return ticks;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
