package urbanthingstest.com.urbanthingstest;

public class Person {

    private int weight;
    private int floor;

    public Person(int weight, int floor) {
        this.weight = weight;
        this.floor = floor;
    }

    public int getFloor() {
        return floor;
    }

    public int getWeight() {
        return weight;
    }
}
